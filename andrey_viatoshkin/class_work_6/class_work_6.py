from datetime import datetime
# 1) Напишите функцию, которая возвращает строку: “Hello world!”


def hello():
    print('Hello World!')

# 2) Напишите функцию, которая вычисляет сумму трех чисел и возвращает
# результат в основную ветку программы.


def sum_of_3(a: int, b: int, c: int):
    return a + b + c


print(sum_of_3(1, 2, 3))


# 3) Придумайте программу, в которой из одной функции вызывается вторая.
# При этом ни одна из них ничего не возвращает в основную ветку программы,
# обе должны выводить результаты своей работы с помощью функции print().


def outer():
    inner()
    print('outer')


def inner():
    print('inner')


outer()
inner()


# 4) Напишите функцию, которая не принимает отрицательные числа. и
# возвращает число наоборот.
# 21445 => 54421
# 123456789 => 987654321


def print_positive_numbers(i: int):

    if i < 0:
        print('число меньше 0')
    else:
        list_1 = [int(k) for k in str(i)]
        list_2 = list_1[::-1]
        print('list_2', list_2)
        return int(''.join(str(n) for n in list_2))


final_int_1 = print_positive_numbers(123456789)
print(final_int_1)
# 5) Напишите функцию fib(n), которая по данному целому неотрицательному
# n возвращает n-e число Фибоначчи.


def fib(n):
    if n < 0:
        print('введите число больше 0')
    elif n == 0:
        print(0)
    elif isinstance(n, int):
        a = 0
        b = 1
        for i in range(n - 2):
            j = a + b
            a = b
            b = j
        print(j)


fib(8)


# 6) Напишите функцию, которая проверяет на то, является ли строка
# палиндромом или нет. Палиндром — это слово или фраза, которые одинаково
# читаются слева направо и справа налево


def if_palindrome():
    word = input('введите слово ')
    list_of_str_2 = [i for i in reversed(word)]
    str_2 = ''.join(i for i in list_of_str_2)
    if word == str_2:
        print(word, 'это палиндром')
    else:
        print(word, 'это не палиндром')


if_palindrome()

# 7) У вас интернет магазин, надо написать функцию которая проверяет что
# введен правильный купон и он еще действителен
# def check_coupon(entered_code, correct_code, current_date,
# expiration_date):
# Code here!
# check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
# check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False


def check_coupon(entered_code, current_date,
                 correct_code='correct_code123',
                 expiration_date='Jul 9, 2015'):
    date_expiration_date = datetime.strptime(expiration_date, '%b %d, %Y')\
        .date()
    date_current_date = datetime.strptime(current_date, '%b %d, %Y').date()
    compare_conditions = entered_code == correct_code and\
        date_expiration_date >= date_current_date
    print(compare_conditions)


check_coupon('correct_code123', 'Jul 10, 2015')


# 8) Фильтр. Функция принимает на вход список, проверяет есть ли эти
# элементы в списке exclude, если есть удаляет их и возвращает список с
# оставшимися элементами
# exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim",
# "Steinbacher"]
#
# filter_list(["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
# "Toulouse", "Blue Swedish"]) => ["Mallard", "Hook Bill", "Crested",
# "Blue Swedish"]

def is_even_1(i):
    exclude = ['African', 'Roman Tufted', 'Toulouse', 'Pilgrim',
               'Steinbacher']
    return i not in exclude


filter_list = ["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
               "Toulouse", "Blue Swedish"]


print(list(filter(is_even_1, filter_list)))
