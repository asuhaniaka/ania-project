import random
# Task 1 Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'

url = 'www.my_site.com#about'
print(url.replace('#', '/'))

# Task 2 В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
name_surname = 'Ivanou Ivan'
name_surname_splited = name_surname.split(' ')

print(name_surname_splited[1], name_surname_splited[0])

# Task 3 Напишите программу которая удаляет пробел в начале строки
test_str = ' xz cx cz xc zxc zxcxc zxc        '


print('test_str with lstrip', len(test_str.lstrip()))

# Task 4  Напишите программу которая удаляет пробел в конце строки
test_str_1 = 'sdasdasdasd d asdsa dasdas dsad s     '


print('test_str_1 with rstrip', len(test_str_1.rstrip()))


# Task 5 a = 10, b=23, поменять значения местами, чтобы в переменную “a”
# было записано значение “23”, в “b” - значение “-10”
a = 10
b = 23
a, b = b, a
print(a, b)

# Task 6 значение переменной “a” увеличить в 3 раза, а значение “b”
# уменьшить на 3
a = 3
b = 4
a *= 3
b -= 3
print(a, b)

# Task 7 преобразовать значение “a” из целочисленного в число с плавающей
# точкой (float), а значение в переменной “b” в строку
a = float(3)
b = str(4.4)
print(a)
print(b)


# Task 8 Разделить значение в переменной “a” на 11 и вывести результат с
# точностью 3 знака после запятой
a = 43434
print(round(a / 11, 3))

# Task 9 Преобразовать значение переменной “b” в число с плавающей точкой и
# записать в переменную “c”. Возвести полученное число в 3-ю степень.
b = float(9.23)
c = pow(b, 3)
print(c)


# Task 10 Получить случайное число, кратное 3-м
random_int = random.randrange(0, 100, 3)
print(random_int)

# Task 11 Получить квадратный корень из 100 и возвести в 4 степень
a = pow(pow(100, 0.5), 4)
print(a)

# Task 12 Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”
salutation = 'Hi guys'
today_str = 'Today'
value_for_task_13 = salutation * 3 + today_str
print(salutation * 3 + today_str)

# Task 13 Получить длину строки из предыдущего задания
print(len(value_for_task_13))

# Task 14 Взять предыдущую строку и вывести слово “Today” в прямом и
# обратном порядке
today_str_straight = today_str[::1]
today_str_backwards = today_str[::-1]

print(today_str_straight)
print(today_str_backwards)


# Task 15 “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и
# обратном порядке
triple_salutation = salutation * 3 + 'Today'
triple_salutation_remove_spaces = triple_salutation.replace(' ', '')
triple_salutation_each_2_character = triple_salutation_remove_spaces[1::2]
triple_salutation_each_2_character_backwards = \
    triple_salutation_remove_spaces[-1::-2]

print(triple_salutation_each_2_character)
print(triple_salutation_each_2_character_backwards)

# Task 16 Используя форматирования подставить результаты из задания 10 и 11
# в следующую строку “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>
# , <в обратном>”
print(f'Task 10: {today_str_straight}, {today_str_backwards} Task 11:'
      f' {triple_salutation_each_2_character} , '
      f'{triple_salutation_each_2_character_backwards}')

# Task 17 Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого
# “name” вставьте ваше имя.
sentence = 'my name is name'
name = 'name'
my_name = 'Andrey'
replace_word_name = sentence.replace('name', my_name)

print(replace_word_name)
replace_word_Andrey = replace_word_name.replace('Andrey', name, 1)
print(replace_word_Andrey)

# Task 18 Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре
print(value_for_task_13.title())
print(value_for_task_13.lower())
print(value_for_task_13.upper())


# Task 19 Посчитать сколько раз слово “Task” встречается в строке из
# задания 12

print(value_for_task_13.count('Task'))
