# if
# Task 1 Если значение некой переменной больше 0, выводилось бы
# специальное сообщение. Один раз выполните программу при значении
# переменной больше 0, второй раз — меньше 0.
a = int(input('Введите число '))


if a > 0:
    print('Больше 0')
elif a < 0:
    print('Меньше 0')

# Task 2 Усовершенствуйте предыдущий код с помощью ветки else так,
# чтобы в зависимости от значения переменной, выводилась либо 1, либо -1.
b = int(input('Введите число '))

if b > 0:
    print('1')
elif b < 0:
    print('-1')

# Task 3 Самостоятельно придумайте программу, в которой бы
# использовалась инструкция if (желательно с веткой elif). Вложенный
# код должен содержать не менее трех выражений.
#
int_1 = int(input('что-то ввели '))

if int_1 > 10:
    print('Большое число')
elif int_1 > 20:
    print('маленькое число')
elif int_1 < 0:
    print('маленькое число')
else:
    print('что-то непонятное')

# ELIF
# 1. Напишите программу по следующему описанию:
# a. двум переменным присваиваются числовые значения;
# b. если значение первой переменной больше второй, то найти разницу
# значений переменных (вычесть из первой вторую), результат связать
# с третьей переменной;
# c. если первая переменная имеет меньшее значение, чем вторая, то третью
# переменную связать с результатом суммы значений двух первых переменных;
# d. во всех остальных случаях, присвоить третьей переменной значение
# первой переменной;

a = int(input('первое число '))
b = int(input('второе число '))

if a > b:
    c = a - b
elif a < b:
    c = a + b
else:
    c = a
print(c)


# Task 2 Придумайте программу, в которой бы использовалась инструкция
# if-elif-else. Количество ветвей должно быть как минимум четыре.

a = int(input('task 2: enter value '))

if a == 5:
    print('first if ', a)
elif a == 6:
    print('first elif ', a)
elif a == 7:
    print('second elif ', a)
elif a > 8:
    print('third elif ', a)


# FOR
# 1. Создайте список, состоящий из четырех строк. Затем, с помощью цикла
# for, выведите строки поочередно на экран.

list_str = ['first row', 'second row', 'third row', 'fourth row']

for row in list_str:
    print(row)


# Task 2. Измените предыдущую программу так, чтобы в конце каждой буквы
# строки добавлялось тире. (Подсказка: цикл for может быть вложен
# в другой цикл.)
for row in list_str:
    print(row, '-')

for row in list_str:
    for add_dash in row:
        add_dash = add_dash + ' -'
    print(row, '-')


# Task 3 Создайте список, содержащий элементы целочисленного типа, затем
# с помощью цикла перебора измените тип данных элементов на числа с
# плавающей точкой. (Подсказка: используйте встроенную функцию float().)

list_of_int = list(range(1, 20))


for element in list_of_int:
    float_element = float(element)
    print(type(float_element), float_element)


# Task 4. Есть два списка с одинаковым количеством элементов, в каждом из
# них этих списков есть элемент “hello”. В первом списке индекс этого
# элемента 1, во втором 7. Одинаковых элементов в обоих списках может быть
# несколько. Напишите программу которая распечатает индекс слова “hello”
# в первом списке и условный номер списка, индекс слова во втором списки
# и условный номер списка со словом Совпадают.


list_1 = ['fdfdf', 'hello', 'tttt', 'aaaa']
list_2 = ['fdfdf', 'tttt', 'aaaa', 'gggg', 'bbbbb', 'y', 'u', 'hello',
          'b']


for hello in list_1:
    if hello == 'hello':
        get_hello_1 = list_1.index(hello)
        print(get_hello_1)
        for hello_2 in list_2:
            if hello_2 == hello:
                get_hello_2 = list_2.index(hello_2)
                print(get_hello_2)
        print(f'Совпадают {get_hello_1}-й элемент из первого списка и'
              f' {get_hello_2}-й элемент из второго списка')


# WHILE
# 1. Напишите скрипт на языке программирования Python, выводящий ряд чисел
# Фибоначчи (числовая последоватьность в которой числа начинаются с 1 и 1
# или же и 0 и 1, пример: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,
# 233, ...). Запустите его на выполнение. Затем измените код так, чтобы
# выводился ряд чисел Фибоначчи, начиная с пятого члена ряда и заканчивая
# двадцатым.

i = 0
int_1 = 0
int_2 = 1
list_for_fib = [int_1]

while i <= 20:
    fib = int_1 + int_2
    int_1 = int_2
    int_2 = fib
    i = i + 1
    list_for_fib.append(int_1)
    if i == 20:
        break

print(list_for_fib[4:20])


# Task 3 2. Напишите цикл, выводящий ряд четных чисел от 0 до 20. Затем,
# каждое третье число в ряде от -1 до -21.

even_number = 0


while even_number < 20:
    print(even_number + 2)
    even_number = even_number + 2

negative_number = -1


while negative_number > -19:
    print(negative_number - 3)
    negative_number = negative_number - 3


# Task 3 Самостоятельно придумайте программу на Python, в которой бы
# использовался цикл while. В комментарии опишите что она делает.

i = 21

while i > 0:
    print('отнимаем единицу', i)
    i = i - 1

# цикл, который за каждый круг отнимает единицу от 21


# input()
# Напишите программу, которая запрашивает у пользователя:
# - его имя (например, "What is your name?")
# - возраст ("How old are you?")
# - место жительства ("Where are you live?")
# После этого выводила бы три строки:
# "This is имя"
# "It is возраст"
# "(S)he lives in место_жительства"

name = input('What is your name? ')
age = input('How old are you? ')
city = input('Where are you live? ')

print(f'This is {str(name)}')
print(f'It is {int(age)}')
print(f'(S)he lives in {str(city)}')


# Напишите программу, которая предлагала бы пользователю решить пример
# 4 * 100 - 54. Потом выводила бы на экран правильный ответ и ответ
# пользователя. Подумайте, нужно ли здесь преобразовывать строку в число.

answer = input('решите пример: 4 * 100 - 54 = ')
answer_int = 4 * 100 - 54
print('правильный ответ - ', answer_int)
print('ваш ответ -  ', answer)


# Запросите у пользователя четыре числа. Отдельно сложите первые два и
# отдельно вторые два. Разделите первую сумму на вторую. Выведите
# результат на экран так, чтобы ответ содержал две цифры после запятой.

first_int = int(input('Введите первое число '))
second_int = int(input('Введите второе число '))
third_int = int(input('Введите третье число '))
fourth_int = int(input('Введите четвертое число '))
fifth_int = first_int + second_int
sixth_int = third_int + fourth_int
seventh_int = round(fifth_int / sixth_int, 2)

print(seventh_int)


# Из заданной строки получить нужную строку:
# "String" => "SSttrriinngg"
# "Hello World" => "HHeelllloo  WWoorrlldd"
# "1234!_ " => "11223344!!__  "


string_for_loop_1 = "String"
for element in string_for_loop_1:
    print(element * 2, end='')

print()
string_for_loop_2 = "Hello World"
for element in string_for_loop_2:
    print(element * 2, end='')

print()
string_for_loop_3 = "1234!_"
for element in string_for_loop_3:
    print(element * 2, end='')


# Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope"
# '50' == "Hot"
# 4 == "Nope"
# "12" == "Nope"
# 60 == "Hot"
print()
x = input('введите число ')
if x == "50":
    print('Hot')
elif x == str(4):
    print('Nope')
elif x == '12':
    print('Nope')
elif x == str(60):
    print('Hot')

x2 = int(input('введите число x2 '))
x2 = x2 ** 2
if x2 > 1000:
    print('Hot')
else:
    print('Nope')


# Сложите все числа в списке, они могут быть отрицательными, если список
# пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050
empty_list = []
list_length = int(input('длинна списка '))

branch_1 = input('вы хотите заполнить список вручную? да/нет ')
for number in range(list_length):
    if branch_1 == 'да':
        n = input('введите число ')
        empty_list.append(float(n))
    else:
        empty_list = range(list_length)
if len(empty_list) == 0:
    print('0')
else:
    print('сумма элементов в списке ', sum(empty_list))


# Подсчет букв
# Дано строка и буква => "fizbbbuz","b", нужно подсчитать сколько раз
# буква b встречается в заданной строке 3
#
# "Hello there", "e" == 3
# "Hello there", "t" == 1
# "Hello there", "h" == 2
# "Hello there", "L" == 2

str_for_counting_1 = 'fizbbbuz'


print(str_for_counting_1.count('b'))


# Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет : и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
list_of_letters_1 = ['a', 'b', 'c', 'd']

for i, num in enumerate(list_of_letters_1, 1):
    print(i, ':', num, end=' ')
print()

# Напишите программу, которая по данному числу n от 1 до n выводит на экран
# n флагов. Изображение одного флага имеет размер 4×4 символов. Внутри
# каждого флага должен быть записан его номер — число от 1 до n.

j = int(input('введите кол-во флагов '))

n = []

for j in range(j):
    n.append(j + 1)
print(n)

for i in range(len(n)):
    print('+___')
    print(f'|{n[i]} /')
    print('|__\\')
    print('|')

# Напишите программу. Есть 2 переменные salary и bonus. Salary - integer,
# bonus - boolean. Если bonus - true, salary должна быть умножена
# на 10. Если false - нет
#
# 10000, True == '$100000'
# 25000, True == '$250000'
# 10000, False == '$10000'
# 60000, False == '$60000'
# 2, True == '$20'
# 78, False == '$78'
# 67890, True == '$678900'

bonus = bool(input('is bonus true or false? '))
salary = int(input('enter your salary '))


if bonus:
    print(salary * 10)
else:
    print(salary)


# Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True

true_list_1 = [1, 1, 1]
false_list_1 = [1, 2, 1]
true_list_2 = ['a', 'a', 'a']
true_list_3 = []

print(all((x == true_list_1[0] for x in true_list_1)))
print(all((x == false_list_1[0] for x in false_list_1)))
print(all((x == true_list_2[0] for x in true_list_2)))
print(all((x == true_list_3[0] for x in true_list_3)))


# Суммирование. Необходимо подсчитать сумму всех чисел до заданного:
# дано число 2, результат будет -> 3(1+2)
# дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
#
# 1 == 1
# 8 == 36
# 22 == 253
# 100 == 5050

i = 100
list_3 = []
while i > 0:
    list_3.append(i)
    i = i - 1
print(sum(list_3))


# Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
#
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]

str_1 = 'dogcat'
str_2 = 'doGCat'
str_3 = input('введите строку ')
list_for_non_empty_str = [False]
list_for_empty_str = [True]
str_3 = [', '.join([letter for letter in str_3 if letter.isupper()])]


if len(str_3) > 0:
    list_for_non_empty_str.append(str_3)
    print(list_for_non_empty_str)
else:
    list_for_empty_str.append(str_3)
    print(list_for_empty_str)
